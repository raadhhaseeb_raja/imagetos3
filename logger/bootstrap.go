package logger

import (
	"os"

	"github.com/siddontang/go-log/log"
)

var l *log.Logger

var filepath = "./log/logrus.log"

func SetPath(path string) {

	filepath = path
}
func Init() {

	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}

	handler, err := log.NewStreamHandler(file)
	if err != nil {
		panic(err)
	}
	l = log.NewDefault(handler)

}

func Instance() *log.Logger {

	return l
}
