// MIT License

package repository

import (
	"github.com/imageToS3/models"
)

type Store interface {
	BeginTx() (Store, error)
	Rollback() error
	CommitTx() error
}

type S3Client interface {
	/////////////////////////////////////////////////////
	///////////////////////////////////////////AWS Client

	UploadImageAndGetURI(image models.UploadImage) (string, error)
	SecurelyUploadImage(image models.UploadImage) (string, error)
}
