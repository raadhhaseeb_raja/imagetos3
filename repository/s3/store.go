package s3

import (
	"fmt"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/imageToS3/conf"
)

var storeOnce sync.Once
var store Store

type Store struct {
	client      *s3.S3
	currSession *session.Session
	AwsS3Region string
	AwsS3Bucket string
	AwsId       string
	AwsKey      string
}

//SharedStore return global or single instance of s3 connection (bounded in sync once)
func SharedStore() Store {
	storeOnce.Do(func() {
		var err error
		store, err = initDb()
		if err != nil {
			panic(err)
		}

	})
	return store
}

func initDb() (Store, error) {

	AWS_S3_REGION := conf.GetConfig().AWS.AWSRegion
	AWS_S3_BUCKET := conf.GetConfig().AWS.AWSBucketName
	AWS_ID := conf.GetConfig().AWS.AccessKeyID
	AWS_KEY := conf.GetConfig().AWS.SecretAccessKey

	creds := credentials.NewStaticCredentials(AWS_ID, AWS_KEY, "")
	_, err := creds.Get()
	if err != nil {
		fmt.Printf("bad credentials: %s", err)
	}
	cfg := aws.NewConfig().WithRegion(AWS_S3_REGION).WithCredentials(creds)

	svc := s3.New(session.New(), cfg) //nolint

	sess, err := session.NewSession(cfg)
	if err != nil {
		panic(err)
	}

	store := Store{
		client:      svc,
		currSession: sess,
		AwsS3Region: AWS_S3_REGION,
		AwsS3Bucket: AWS_S3_BUCKET,
		AwsId:       AWS_ID,
		AwsKey:      AWS_KEY,
	}

	return store, nil
}
