package s3

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/imageToS3/conf"
	"github.com/imageToS3/models"
)

func (s *Store) UploadImageAndGetURI(image models.UploadImage) (string, error) {

	// packaging the input into params
	params := &s3.PutObjectInput{
		Bucket:        aws.String(s.AwsS3Bucket),
		Key:           aws.String(image.Path),
		Body:          image.FileBytes,
		ContentLength: aws.Int64(image.FileSize),
		ContentType:   aws.String(image.FileType),
	}
	// sending the image to s3 server based on the above configs
	_, err := s.client.PutObject(params)
	// if error, returns empty URI and error
	if err != nil {
		return "", err
	}

	// packaging the uri of image into proper format
	url := fmt.Sprintf(conf.GetConfig().AWS.ImageURL, s.AwsS3Bucket, s.AwsS3Region, image.Path)

	// returns the url to service layer and then to controller layer for further operation
	return url, nil
}

func (s *Store) SecurelyUploadImage(image models.UploadImage) (string, error) {
	// packaging the input into params
	params := &s3.PutObjectInput{
		Bucket:        aws.String(s.AwsS3Bucket),
		Key:           aws.String(image.Path),
		Body:          image.FileBytes,
		ContentLength: aws.Int64(image.FileSize),
		ContentType:   aws.String(image.FileType),
	}
	// sending the image to s3 server based on the above configs
	_, err := s.client.PutObject(params)
	// if error, returns empty URI and error
	if err != nil {
		return "", err
	}
	// packaging the uri of image into proper format
	url := fmt.Sprintf(conf.GetConfig().AWS.ImageURL, s.AwsS3Bucket, s.AwsS3Region, image.Path)

	// returns the url to service layer and then to controller layer for further operation
	return url, nil
}
