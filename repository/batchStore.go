package repository

import (
	"context"

	"github.com/imageToS3/models"
)

type BatchStore interface {
	BatchProcess(context context.Context, batchJob *models.BatchJobs) error
}
