// MIT License

package rest

import (
	"github.com/imageToS3/logger"
	"github.com/imageToS3/service"
)

//StartServer initate server
func StartServer(container *service.Container) *HttpServer {

	//Inject services instance from ServiceContainer

	s3Controller := NewS3Controller(container.S3Service)

	httpServer := NewHttpServer(
		container.GbeConfigService.GetConfig().RestServer.Addr,
	)
	//Inject controller instance to server
	httpServer.s3Controller = s3Controller

	go httpServer.Start()
	logger.Instance().Info("rest server ok")
	return httpServer
}
