// MIT License

package rest

import (
	"io/ioutil"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type HttpServer struct {
	addr         string
	s3Controller S3Controller
}

//NewHttpServer create server instance
func NewHttpServer(addr string,
) *HttpServer {
	return &HttpServer{
		addr: addr,
	}
}

func (server *HttpServer) Start() {
	gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = ioutil.Discard

	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	public := r.Group("/v1")
	{
		public.POST("/api/auth/personal/secureUploadImages", server.s3Controller.SecurelyUploadImages)
		public.POST("/api/auth/personal/image/public", server.s3Controller.UploadPersonalPublicImage)
	}

	err := r.Run(server.addr)
	if err != nil {
		panic(err)
	}

}
