package rest

import (
	"bytes"
	"net/http"
	"strings"

	"github.com/imageToS3/models"
	"github.com/imageToS3/service"
	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
)

type S3Controller interface {
	UploadPersonalPublicImage(ctx *gin.Context)
	SecurelyUploadImages(ctx *gin.Context)
}

type s3Controller struct {
	s3Service service.S3Service
}

func NewS3Controller(s3Service service.S3Service) S3Controller {
	return &s3Controller{
		s3Service: s3Service,
	}
}

func (s *s3Controller) UploadPersonalPublicImage(ctx *gin.Context) {
	// get *multipart.Form from request
	form, err := ctx.MultipartForm()
	// if there is an error, throw relevant error
	if err != nil {
		ctx.JSON(http.StatusOK, NewStandardResponse(false, models.INTERNAL_SERVER_ERROR, models.INTERNAL_SERVER_ERROR_MESSAGE, nil))
		return
	}
	images := form.File["image"]
	var allURI []string
	for _, image := range images {
		// open the image and return the associated file
		img, err := image.Open()
		// if there is an error, return error
		if err != nil {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.INTERNAL_SERVER_ERROR, models.INTERNAL_SERVER_ERROR_MESSAGE, nil))
			return
		}
		// get size of image that we received in context
		size := image.Size
		// creates an array of byte of the size of image
		buffer := make([]byte, size)

		// Read populates the given byte slice with data and returns the number of bytes populated and an error value.
		// It returns an io.EOF error when the stream ends.
		// since we only want to populate our buffer with data, we have assigned _ to return values
		_, _ = img.Read(buffer)
		// converts our data into *bytes.Reader so that s3 can read and seek data
		fileBytes := bytes.NewReader(buffer)
		// detects file type from the buffer
		// fileName := image.Filename
		fileType := strings.Split(image.Filename, ".")[1]
		// creating a path for our image, here
		// 	/media/ is the folder and filename is the name of file that was sent in context
		path := "/media/" + xid.New().String() + image.Filename
		err = models.ValidatePersonalImage(size, fileType)
		if err != nil {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.IMAGE_UPLOAD_FAILED, err.Error(), nil))
			return
		}
		// packaging our data into a struct to easily pass to other layers
		uploadImage := models.UploadImage{
			FileBytes: fileBytes,
			FileType:  fileType,
			FileSize:  size,
			Path:      path,
		}
		// passing the uploadImage struct to service layer which will then
		//	communicate with repository/domain layer and uploads and returns
		//	the uri of image.
		uri, err := s.s3Service.UploadImageAndGetURI(uploadImage)
		// if there is an error, return error
		if err != nil {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.IMAGE_UPLOAD_FAILED, models.IMAGE_UPLOAD_FAILED_MESSAGE, nil))
			return
		}
		// if uri is empty, return error
		if uri == "" {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.URI_FETCHING_FAILED, models.URI_FETCHING_FAILED_MESSAGE, nil))
			return
		}
		allURI = append(allURI, uri)
	}

	uriVo := GetNewURI(allURI)
	ctx.JSON(http.StatusOK, NewStandardResponse(true, models.SUCCESS, models.URI_FETCH_SUCCESS_MESSAGE, uriVo))

}
func (s *s3Controller) SecurelyUploadImages(ctx *gin.Context) {
	// get *multipart.Form from request
	form, err := ctx.MultipartForm()
	// if there is an error, throw relevant error
	if err != nil {
		ctx.JSON(http.StatusOK, NewStandardResponse(false, models.INTERNAL_SERVER_ERROR, models.INTERNAL_SERVER_ERROR_MESSAGE, nil))
		return
	}
	images := form.File["secureImage"]
	var allURI []string
	for _, image := range images {
		// open the image and return the associated file
		img, err := image.Open()
		// if there is an error, return error
		if err != nil {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.INTERNAL_SERVER_ERROR, models.INTERNAL_SERVER_ERROR_MESSAGE, nil))
			return
		}
		// get size of image that we received in context
		size := image.Size
		// creates an array of byte of the size of image
		buffer := make([]byte, size)

		// Read populates the given byte slice with data and returns the number of bytes populated and an error value.
		// It returns an io.EOF error when the stream ends.
		// since we only want to populate our buffer with data, we have assigned _ to return values
		_, _ = img.Read(buffer)

		// converts our data into *bytes.Reader so that s3 can read and seek data
		fileBytes := bytes.NewReader(buffer)
		// detects file type from the buffer
		// fileType := http.DetectContentType(buffer)
		fileType := strings.Split(image.Filename, ".")[1]

		// creating a path for our image, here
		// 	/media/ is the folder and filename is the name of file that was sent in context
		path := "/secureMedia/" + xid.New().String() + image.Filename
		err = models.ValidateStoreImage(size, fileType)
		if err != nil {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.IMAGE_UPLOAD_FAILED, err.Error(), nil))
			return
		}
		// packaging our data into a struct to easily pass to other layers
		uploadImage := models.UploadImage{
			FileBytes: fileBytes,
			FileType:  fileType,
			FileSize:  size,
			Path:      path,
		}
		// passing the uploadImage struct to service layer which will then
		//	communicate with repository/domain layer and uploads and returns
		//	the uri of image.
		uri, err := s.s3Service.SecurelyUploadImage(uploadImage)
		// if there is an error, return error
		if err != nil {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.IMAGE_UPLOAD_FAILED, models.IMAGE_UPLOAD_FAILED_MESSAGE, nil))
			return
		}
		// if uri is empty, return error
		if uri == "" {
			ctx.JSON(http.StatusOK, NewStandardResponse(false, models.URI_FETCHING_FAILED, models.URI_FETCHING_FAILED_MESSAGE, nil))
			return
		}
		allURI = append(allURI, uri)
	}

	uriVo := GetNewURI(allURI)
	ctx.JSON(http.StatusOK, NewStandardResponse(true, models.SUCCESS, models.URI_FETCH_SUCCESS_MESSAGE, uriVo))

}
