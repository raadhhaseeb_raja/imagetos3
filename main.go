package main

import (
	"fmt"
	"net/http"

	"github.com/imageToS3/conf"
	"github.com/imageToS3/logger"
	"github.com/imageToS3/rest"
	"github.com/imageToS3/service"
	"github.com/siddontang/go/log"
)

func main() {
	logger.Init()

	conf.SetConfFilePath("./")

	logger.Instance().Info("#==================================#")
	logger.Instance().Info("#===========Starting Server =======#")
	logger.Instance().Info("#==================================#")

	go func() {
		log.Info(http.ListenAndServe("localhost:6080", nil))
	}()
	/*
	* Initiate Service Layer Container
	 */

	serviceContainer := service.NewServiceContainer()

	/*
	* Initiate Rest Server
	 */
	rest.StartServer(serviceContainer)

	fmt.Println("========== Rest Server Started ============")

	/*
	* Initiate GRPC Server
	 */
	// grpc.StartServer(serviceContainer)

	fmt.Println("========== RPC Server Started =============")

	/*
	* Initiate Kafka Engine
	 */
	// userengine.StartEngine()
	// businessengine.StartEngine()
	// fmt.Println("========== Engine Started =============")

	/*
	* Initiate Kafka Log processor
	 */
	// writers.NewKafkaLogReader(gbeConfig.Kafka.Brokers).Start()

	fmt.Println("========== Server Stated ============")

	select {}

}
