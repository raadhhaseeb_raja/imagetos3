// MIT License

package conf

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"sync"
	"time"
)

var (
	configPath     = "."
	configFileName = "conf.json"
)

type GbeConfig struct {
	BaseURL          string                  `json:"baseURL"`
	PostDataSource   PostgreDataSourceConfig `json:"postgreDataSource"`
	SiteBaseURL      string                  `json:"siteBaseURL"`
	AWS              AWS                     `json:"aws"`
	FireBase         FireBaseConfig          `json:"fireBase"`
	ProjectInfo      ProjectInfo             `json:"projectInfo"`
	ProjectName      string                  `json:"projectName"`
	RestServer       RestServerConfig        `json:"restServer"`
	SecretMainServer SecretMainServerConfig  `json:"secretMainServer"`
}

type AWS struct {
	AccessKeyID     string `json:"accessKeyId"`
	SecretAccessKey string `json:"secretAccessKey"`
	AWSRegion       string `json:"awsRegion"`
	AWSBucketName   string `json:"awsBucketName"`
	ImageURL        string `json:"imageURL"`
}
type PostgreDataSourceConfig struct {
	DriverName        string `json:"driverName"`
	Addr              string `json:"addr"`
	Port              string `json:"port"`
	Database          string `json:"database"`
	User              string `json:"user"`
	Password          string `json:"password"`
	EnableAutoMigrate bool   `json:"enableAutoMigrate"`
	Retries           int64  `json:"retries"`
}

type FireBaseConfig struct {
	App                   AppConfig `json:"app"`
	AppDynamicLinkDomain  string    `json:"appDynamicLinkDomain"`
	DynamicLinkProjectKey string    `json:"dynamicLinkProjectKey"`
	MessgingKey           string    `json:"messagingKey"`
	ProjectKey            string    `json:"projectKey"`
	StorageName           string    `json:"storageName"`
	URL                   string    `json:"url"`
	WebDynamicLinkDomain  string    `json:"webDynamicLinkDomain"`
}

type AppConfig struct {
	AndroidAppId string `json:"androidAppId"`
	IosAppId     string `json:"iosAppId"`
}

type ProjectInfo struct {
	Name string `json:"name"`
}

type SecretMainServerConfig struct {
	Addr string `json:"addr"`
}

type RestServerConfig struct {
	Addr string `json:"addr"`
}

type ServerConfig struct {
	ServerMinTime time.Duration
	ServerTime    time.Duration
	ServerTimeout time.Duration
}

var config GbeConfig
var configOnce sync.Once

func SetConfFilePath(path string) {
	configPath = path
}

func SetConfFileName(name string) {
	configFileName = name
}

func GetConfig() *GbeConfig {
	configOnce.Do(func() {

		bytes, err := ioutil.ReadFile(configPath + "/" + configFileName)
		log.Println(configPath + "/" + configFileName)
		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(bytes, &config)
		if err != nil {
			panic(err)
		}
	})
	log.Println(config.PostDataSource)
	return &config
}
