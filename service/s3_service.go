package service

import (
	"github.com/imageToS3/models"
	"github.com/imageToS3/repository/s3"
)

type S3Service interface {
	UploadImageAndGetURI(image models.UploadImage) (string, error)
	SecurelyUploadImage(image models.UploadImage) (string, error)
}

type s3Service struct {
	store s3.Store
}

func NewS3Service(store s3.Store) s3Service {
	return s3Service{store: store}
}

func (s s3Service) UploadImageAndGetURI(image models.UploadImage) (string, error) {
	return s.store.UploadImageAndGetURI(image)
}

func (s s3Service) SecurelyUploadImage(image models.UploadImage) (string, error) {
	return s.store.SecurelyUploadImage(image)
}
