package service

import (
	"github.com/imageToS3/logger"
	"github.com/siddontang/go-log/log"
)

type LoggerService interface {
	GetInstance() *log.Logger
}

type loggerService struct {
	logger *log.Logger
}

func NewLoggerService() LoggerService {
	logger.Init()
	return &loggerService{
		logger: logger.Instance(),
	}

}

func (log *loggerService) GetInstance() *log.Logger {
	return log.logger
}
