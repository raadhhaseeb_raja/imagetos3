package service

import (
	"github.com/imageToS3/repository/s3"

	"github.com/imageToS3/repository"
	postgres "github.com/imageToS3/repository/postgres"
	"github.com/imageToS3/utils"
)

type Container struct {
	GbeConfigService GbeConfigService
	LoggerService    LoggerService
	Store            repository.Store
	S3Service        S3Service
	Logger           utils.Logger
}

func NewServiceContainer() *Container {
	//gbeConfig := conf.GetConfig()
	gbeConfigService := NewGbeConfigService()

	// store := firebase.SharedStore()
	///	batchStore := batch.SharedStore()
	postgreStore := postgres.SharedStore()
	s3Store := s3.SharedStore()
	//httpClient := lib.GetHttpClient()
	logger := utils.NewLogger()
	loggerService := NewLoggerService()

	s3Service := NewS3Service(s3Store)

	return &Container{

		GbeConfigService: gbeConfigService,

		LoggerService: loggerService,

		Store: postgreStore,

		S3Service: s3Service,

		Logger: logger,
	}
}
