package service

import "testing"

func TestNewLoggerService(t *testing.T) {
	t.Run("Should return logger instance", func(t *testing.T) {
		NewTestService(t)
		loggerService := NewLoggerService()
		logger := loggerService.GetInstance()

		if logger == nil {
			t.Fail()
		}
	})
}
