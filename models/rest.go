package models

import (
	"bytes"
	"fmt"
)

////////////////////////////////////////////////////////
///////////////////// Images Request ///////////////////
////////////////////////////////////////////////////////

type UploadImage struct {
	FileBytes *bytes.Reader
	FileType  string
	FileSize  int64
	Path      string
}

/////////////////////////////////////////////////////////
///////////////  HTTP REQUESTS  /////////////////////////
////////////////////////////////////////////////////////
type PersonalSignUpRequest struct {
	DialingCode string `json:"dialingCode"`
	Phone       string `json:"phone"`
	Email       string `json:"email"`
	PassCode    string `json:"passCode"`
	LastName    string `json:"lastName"`
	FirstName   string `json:"firstName"`
	ProfileKey  string `json:"profileKey"`
	CountryCode string `json:"countryCode"`
}

// type StoreSignUpRequest struct {
// 	StoreName                  string `json:"storeName"`
// 	BusinessRegistrationNumber string `json:"businessRegistrationNumber"`
// 	DeliveryAgencyBank         struct {
// 		DeliverAgencyName          string `json:"deliverAgencyName"`
// 		BankName                   string `json:"bankName"`
// 		BankAccountNumber          string `json:"bankAccountNumber"`
// 		AccountHolderName          string `json:"accountHolderName"`
// 		AccountHolderContactNumber string `json:"accountHolderContactNumber"`
// 	} `json:"deliveryAgencyBank"`
// 	Email string `json:"email"`
// }

type BusinessAddRequest struct {
	BusinessTagname     string `json:"businessTagName"`
	CountryCode         string `json:"countryCode"`
	Description         string `json:"description"`
	KYBId               string `json:"kybId"`
	BusinessPhoneNumber string `json:"businessPhoneNumber"`
	Address             string `json:"address"`
}

type FreeLancerSignUpRequest struct {
	Address             string `json:"address"`
	BusinessPhoneNumber string `json:"businessPhoneNumber"`
	BusinessTagname     string `json:"businessTagName"`
	CountryCode         string `json:"countryCode"`
	Description         string `json:"description"`
	FCMToken            string `json:"fcmToken"`
	KYBId               string `json:"kybId"`
	Language            string `json:"language"`
	PassCode            string `json:"passCode"`
	Referral            string `json:"referral"`
	UDID                string `json:"udid"`
}

type FreeLancerAddRequest struct {
	BusinessTagname     string `json:"businessTagName"`
	CountryCode         string `json:"countryCode"`
	Description         string `json:"description"`
	KYBId               string `json:"kybId"`
	BusinessPhoneNumber string `json:"businessPhoneNumber"`
	Address             string `json:"address"`
}

type BODSignUpRequest struct {
	BODId       string `json:"bodId"`
	BusinessId  string `json:"businessId"`
	CountryCode string `json:"countryCode"`
	FCMToken    string `json:"fcmToken"`
	Language    string `json:"language"`
	PassCode    string `json:"passCode"`
	Referral    string `json:"referral"`
	UDID        string `json:"udid"`
}
type RegisterStoreReq struct {
	StoreName                   string `json:"storeName"`
	BusinessRegistrationNumber  string `json:"businessRegistrationNumber"`
	DeliveryAgencyName          string `json:"deliveryAgencyName"`
	BankName                    string `json:"bankName"`
	BankAccountNumber           string `json:"bankAccountNumber"`
	AccountHolder               string `json:"accountHolder"`
	AccountHolderBusinessNumber string `json:"accountHolderBusinessNumber"`
	BusinessLicensePaperKey     string `json:"businessLicensePaperKey"`
	RepresentativeIdCardKey     string `json:"representativeIdCardKey"`
	BankAccountCopyKey          string `json:"bankAccountCopyKey"`
	BusinessEmail               string `json:"bsusinessEmail"`
	StoreLogoKey                string `json:"storeLogoKey"`
	StoreFrontPictureKey        string `json:"storeFrontPictureKey"`
	KilingMenuShotKey           string `json:"kilingMenuShotKey"`
	RequiredTerms               bool   `json:"requiredTerms"`
	UUID                        string `json:"uuid"`
	StoreID                     string `json:"storeId"`
}
type BODInvitationData struct {
	Countrycode string `json:"countryCode"`
	Dialingcode string `json:"dialingcode"`
	ID          string `json:"id"`
	Phone       string `json:"phone"`
}

type InviteBodRequest struct {
	BusinessId string              `json:"businessId"`
	Bods       []BODInvitationData `json:"bods"`
}

/////////////////////////////////////////////////////////
///////////////  SYSTEM MODELS  /////////////////////////
////////////////////////////////////////////////////////
type Passcode struct {
	Passcode string `json:"passcode"`
}

////////////////////////////////////////////////////////
///////////////  CUSTOM TYPES //////////////////////////
////////////////////////////////////////////////////////
type AccountType string
type LoginType string

func (t AccountType) String() string {
	return string(t)
}
func (t LoginType) String() string {
	return string(t)
}

//NewAccountTypeFromString return error if passed string is not one of PERSONAL, BUSINESS
func NewAccountTypeFromString(s string) (*AccountType, error) {
	accountType := AccountType(s)
	switch accountType {

	case AccountTypeBusiness:
	case AccountTypePersonal:
	case AccountTypeFeeLancer:
	default:
		return nil, fmt.Errorf("invalid account type: %v", s)
	}
	return &accountType, nil
}

func NewLoginTypeFromString(s string) (*LoginType, error) {
	loginType := LoginType(s)
	switch loginType {

	case LoginTypeEmail:
	case LoginTypePhone:
	default:
		return nil, fmt.Errorf("invalid login type: %v", s)
	}
	return &loginType, nil
}

type Subject string
type StoreStatus string

func (t Subject) String() string {
	return string(t)
}
func (t StoreStatus) String() string {
	return string(t)
}

//NewAccountTypeFromString return error if passed string is not one of PERSONAL, BUSINESS
func NewSubjectFromString(s string) (*Subject, error) {
	subject := Subject(s)
	switch subject {

	case SignUp:
	case FogetPassCode:
	case BusinessEmail:
	default:
		return nil, fmt.Errorf("invalid account type: %v", s)
	}
	return &subject, nil
}

func NewStoreStatusFromString(s string) (*StoreStatus, error) {
	status := StoreStatus(s)
	switch status {
	case StoreSuspended:
	case StoreEnabled:
	default:
		return nil, fmt.Errorf("invalid account type: %v", s)
	}
	return &status, nil
}

////////////////////////////////////////////////
///////////////////////////////////////////////
type KYCStatus string

func (t KYCStatus) String() string {
	return string(t)
}

//NewJackPotStatusFromString return error if passed string is not one of enabled,disabled
func NewKYCStatus(s string) (*KYCStatus, error) {
	status := KYCStatus(s)
	switch status {
	case KYCStatusNew:
	case KYCStatusDocumentRequest:
	case KYCStatusInReview:
	case KYCStatusRejected:
	case KYCStatusFinalRejected:
	case KYCStatusApproved:

	default:
		return nil, fmt.Errorf("invalid status: %v", s)
	}
	return &status, nil
}

type UserStatus string

type DeviceStatus string

type CurrencyType string

func (t UserStatus) String() string {
	return string(t)
}

func NewUserStatus(s string) (*UserStatus, error) {
	status := UserStatus(s)
	switch status {
	case UserStatusEnabled:
	case UserStatusFinalRejectedWithdrawFunds:

	default:
		return nil, fmt.Errorf("invalid status: %v", s)
	}
	return &status, nil
}

func NewDeviceStatus(s string) (*DeviceStatus, error) {
	status := DeviceStatus(s)
	switch status {
	case DeviceStatusActive:
	case DeviceStatusDisable:
	default:
		return nil, fmt.Errorf("invalid status: %v", s)
	}
	return &status, nil
}

type BusinessStatus string

func (t BusinessStatus) String() string {
	return string(t)
}

func NewBusinessStatus(s string) (*BusinessStatus, error) {
	status := BusinessStatus(s)
	switch status {
	case BusinessStatusEnabled:
	case BusinessStatusBoardDirectorOnHold:

	default:
		return nil, fmt.Errorf("invalid status: %v", s)
	}
	return &status, nil
}

//////////////////////////////////////////////////////////////////////
////////////////////// EMAIL AUTH API REQUESTS///////////////////
/////////////////////////////////////////////////////////////////////

type ChangePhoneNumberRequest struct {
	NewPhoneNumber string `json:"newPhoneNumber"`
	CountryCode    string `json:"CountryCode"`
}
type ChangeFullNameRequest struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

type SignInRequest struct {
	DialingCode string `json:"dialingCode"`
	Phone       string `json:"phone"`
	Email       string `json:"email"`
	PassCode    string `json:"passCode"`
	CountryCode string `json:"countryCode"`
	LoginType   string `json:"loginType"`
}
type UpdatePassCodeRequest struct {
	Token    string `json:"token"`
	PassCode string `json:"passCode"`
}

type CheckUserFullNameRequest struct {
	Token    string `json:"token"`
	FullName string `json:"fullName"`
}
type BiometricSignInRequest struct {
	UDID string `json:"udid"`
}

type DeviceAuthRequest struct {
	UDID string `json:"udid"`
}

type UserAccountsDetail struct {
	UserName    string `json:"userName"`
	UID         string `json:"uid"`
	FullName    string `json:"fullName"`
	AccountType string `json:"accountType"`
}

type ChangeUsernameRequest struct {
	NewUsername string `json:"newUsername"`
}

////////////////////////////////////////////////////////////

type S3Request struct {
	Key string `json:"key"`
}
type EmailAuthReq struct {
	Email string `json:"email" validate:"required,email"`
}

type PersonalEmailCodeVerificationRequest struct {
	Email string `json:"email"`
	Code  string `json:"code"`
}

type SetLanguageRequest struct {
	Language string `json:"language"`
}

/////////////////////////////////////////////////////////////
type PhoneAuthReq struct {
	Phone       string `json:"phone"`
	DialingCode string `json:"dialingCode"`
	CountryCode string `json:"countryCode"`
}

// type ForgetPassCodePhoneAuthVerifyReq struct {
// 	Phone       string `json:"phone"`
// 	DialingCode string `json:"dialingCode"`
// 	Code        string `json:"code"`
// 	CountryCode string `json:"countryCode"`
// }

type BusinessStoreCheckReq struct {
	StoreName                  string `json:"storeName"`
	BusinessRegistrationNumber string `json:"businessRegistrationNumber"`
}

type BusinessBankDetailsReq struct {
	DeliveryAgencyName    string `json:"deliveryAgencyName"`
	BankName              string `json:"bankName"`
	BankAccountNumber     string `json:"bankAccountNumber"`
	AccountHolder         string `json:"accountHolder"`
	AccountHolderResident string `json:"accountHolderResident"`
}

/////////////////////////////////////////////////////////////
type VerifyPhoneAuthReq struct {
	Phone       string `json:"phone"`
	Code        string `json:"code"`
	DialingCode string `json:"dialingCode"`
	CountryCode string `json:"countryCode"`
}

////////////////////////////////////////////////////////////
type ChangeLostPhoneNumber struct {
	Token string `json:"token"`
	UDID  string `json:"udid"`
}

////////////////////////////////////////////////////////////
type OTPLostPhoneNumber struct {
	Token       string `json:"token"`
	CountryCode string `json:"countryCode"`
	PhoneNumber string `json:"phoneNumber"`
}

///////////////////////////////////////////////////////////
type OTPVerifyLoastPhoneNumber struct {
	Token string `json:"token"`
	OTP   string `json:"otp"`
	UDID  string `json:"udid"`
}

type ForgetPhoneNumberReq struct {
	NewPasscode string `json:"newPassCode"`
	UDID        string `json:"udid"`
	Code        string `json:"code"`
}
