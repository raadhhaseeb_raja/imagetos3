package models

import "time"

type SMS struct {
	Phone string `json:"phone"`
	Body  string `json:"body"`
}

type SmsAuth struct {
	Code  string `json:"code"`
	Phone string `json:"phone"`
}
type UserSMS struct {
	Phone       string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	Code        string
	SubjectType Subject
	Verified    bool
}
