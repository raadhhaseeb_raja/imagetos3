package models

const (
	// MaxSize  Maximum Allowed Size to upload images
	MaxSize = 15000000
)
const (
	// RFC 3339 is also supported
	TimeFormat = "2006-01-02T15:04:05-0700" // ISO 8601 Standard
)
