package models

import (
	"fmt"
	validator "github.com/go-playground/validator/v10"
)

//Every request struct needs to implement this interface
type RequestValidation interface {
	Validate() error
}

var validate *validator.Validate

/////////////////////////////////////////
////////////////////////////////////////

func ValidateStoreImage(size int64, contentType string) error {
	imageContent := [4]string{"image/jpg", "image/png", "image/jpeg", "image/gif"}
	flag := false
	for _, content := range imageContent {
		if content == "image/"+contentType {
			flag = true

		}
	}
	if !flag {
		return fmt.Errorf("image content type not supported")
	}
	kilobytes := (int64)(size / 1024)
	megaBytes := (float64)(kilobytes / 1024)
	if megaBytes > 15 {
		return fmt.Errorf("image size size exceeded")
	}

	return nil
}
func ValidatePersonalImage(size int64, contentType string) error {
	imageContent := [4]string{"image/jpg", "image/png", "image/jpeg", "image/gif"}
	flag := false
	for _, content := range imageContent {
		if content == "image/"+contentType {
			flag = true

		}
	}
	if !flag {
		return fmt.Errorf("image content type not supported")
	}
	kilobytes := (int64)(size / 1024)

	megaBytes := (float64)(kilobytes / 1024)
	if megaBytes > 5 {
		return fmt.Errorf("image size exceeded")
	}

	return nil
}
