package models

const (
	INTERNAL_SERVER_ERROR = 500
	SUCCESS               = 1000
	IMAGE_UPLOAD_FAILED   = 1016
	URI_FETCHING_FAILED   = 1017
	IMAGE_SIZE_EXCEEDED   = 1018
)

const (
	///////////////////////////////
	//////////////Image Uploading//
	///////////////////////////////
	IMAGE_UPLOAD_FAILED_MESSAGE = "Failed to upload image to repository"
	URI_FETCHING_FAILED_MESSAGE = "Failed to retrieve the URI of Image"
	URI_FETCH_SUCCESS_MESSAGE   = "Successfully uploaded and retrieved the URI of Image"
	IMAGE_SIZE_EXCEEDED_MESSAGE = "One of the images exceed the maximum allowed size of 15 MB"

	///////////////////////////////
	INTERNAL_SERVER_ERROR_MESSAGE = "Request faild with internal server error."
	INVALID_INPUT_MESSAGE         = "Invalid Input."
)
